//
//  ViewController.swift
//  localizationTest
//
//  Created by AlexanderKogut on 10/3/18.
//  Copyright © 2018 AlexanderKogut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var labeli: UILabel!
    @IBOutlet weak var firstB: UIButton!
    @IBOutlet weak var secondB: UIButton!
    @IBOutlet weak var thirdB: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("mainNavigation", comment: "навигация")
        setTitel()
    }

    func setTitel() {
        
        labeli.text = NSLocalizedString("mainLabel", comment: "")
        firstB.setTitle(NSLocalizedString("mainButton1", comment: ""), for: .normal)
        secondB.setTitle(NSLocalizedString("mainButton2", comment: ""), for: .normal)
        thirdB.setTitle(NSLocalizedString("mainButton3", comment: ""), for: .normal)
    }
}

